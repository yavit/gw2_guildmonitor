const request = require('request');
const rp = require('request-promise-native');
const fs = require('fs');

const CONFIG_PATH = './config.json';
const SAVE_FILE = 'last_id.json';
const CONFIG = JSON.parse(fs.readFileSync(CONFIG_PATH));
const API_URI = {
  base: 'https://api.guildwars2.com',
  account: '/v2/account',
  guild: '/v2/guild',
  item: '/v2/items'
};

function parse(logs){
  readableLogs = logs.map(async (log)=>{
    let itemUrl = `${API_URI.item}/${log.item_id}`;
    switch(log.type){
      case 'joined':
        return `${log.user} à rejoins la guilde.`;
      case 'invited':
        return `${log.invited_by} à invité ${log.user}.`;
      case 'invite_declined':
        return `${log.user} à refusé l'invitation en guilde.`;
      case 'kick':
        return `${log.kicked_by} à kick ${log.user} de la guilde.`;
      case 'rank_change':
        return `${log.changed_by} à changé le rang de ${log.user}. ${log.old_rank} -> ${log.new_rank}`;
      case 'treasury':
        //TODO Find item
        return await requestAPI(itemUrl)
          .then((res)=>{
            return `${log.user} a déposé ${log.count} ${res.name} ${res.chat_link} dans le trésor de guilde. `;
          })
          .catch((err)=>{
            return `${log.user} a déposé ${log.count} id-${log.item_id} dans le trésor de guilde.`;
          });
      case 'stash':
        return await requestAPI(itemUrl)
          .then((res)=>{
            let operation = null;
            switch (log.operation) {
              case 'deposit':
                operation = 'déposé';
                break;
              case 'withdraw':
                operation = 'retiré';
                break;
              case 'move':
                operation = 'déplacé';
                break;
            }
            return `${log.user} a ${operation} ${log.count} ${res.name} ${res.chat_link} dans le dépot de guilde. `;
          }).catch((err)=>{
            //console.error(err);
            return `${log.user} a effectué une opération dans le dépot de guilde (${log.count} id-${log.item_id}).`;
          });

      case 'motd':
        return `${log.user} à changé le message du jour : <<${log.motd}>>.`;
      case 'upgrade':
        let action = null;
        switch(log.action){
          case 'queued':
            action = 'mis en fille d\'attente';
            break;
          case 'cancelled':
            action = 'annulé';
            break;
          case 'completed':
            action = 'complété';
            break;
          case 'sped_up':
            action = 'accéléré';
            break;
        }
        return `${log.user} à ${action} l'upgrade de guilde ${log.upgrade_id}.`;
      default:
        return `Une action non prévue à été faite : ${log} `;
    }
  });
  Promise.all(readableLogs)
    .finally(()=>{
      console.log('readableLogs 2', readableLogs);
    });

}

function requestAPI(url){
  console.debug('requestAPI', url);
  var options = {
    uri: `${API_URI.base}${url}`,
    headers: {
      'User-Agent': 'Request-Promise',
      'Authorization': `Bearer ${CONFIG.guild_api_key}`,
      'Accept-Language': 'FR'
    },
    json: true // Automatically parses the JSON string in the response
  };

  return rp(options);
}

class Watcher{
  constructor(){
    this.guild = null;
  }

  initialize(){
    console.debug('initialize');
    const accountPromise = requestAPI(API_URI.account)
      .then((data)=>{
        return this.findGuildId(data.guild_leader);
      })
      .catch((err)=>{
        console.error(`Error: ${err}`);
        return null;
      });
    accountPromise.then(()=>{
      this.update();
    })
  }

  update(){
    console.debug('Update');
    if(!this.guild){
      console.debug('No guild found, schedule new initialize');
      setTimeout(this.initialize(), CONFIG.interval);
    }

    const lastUpdate = this.readLastId();
    console.log('LAST UPDATE', lastUpdate);
    let url = `${API_URI.guild}/${this.guild}/log`;
    if(lastUpdate){
      //TODO
      url = `${url}?since=${lastUpdate}`;
    }

    requestAPI(url)
      .then((data)=>{
        if(data.length){
          this.saveLastId(data[0].id);
        }
        parse(data);
      })
      .catch((err)=>{
        console.error('Error requesting API', err)
      })
  }

  findGuildId(guilds){
    console.debug('findGuildId');
    const guildPromiseArray = [];
    guilds.forEach((leader)=>{
      guildPromiseArray.push(requestAPI(`${API_URI.guild}/${leader}`)
        .then((data)=>{
          if(data.tag === CONFIG.guild_tag){
            console.debug('GUILD FOUND');
            this.guild = data.id;
          }
        })
        .catch((err)=>{
          console.error('ERROR', err);
          //TODO HANDLE ERROR
        })
      );
    });
    return Promise.all(guildPromiseArray)
      .finally(()=>{
        return Promise.resolve();
      })
  }

  static readLastId(){
    console.debug('readLastId');
    if (fs.existsSync(SAVE_FILE)) {
      return JSON.parse(fs.readFileSync(SAVE_FILE)).id;
    }
    return null;
  }

  saveLastId(id){
    console.debug('saveLastId');
    const json = JSON.stringify({id: id});
    return fs.writeFile(SAVE_FILE, json, (err)=>{
      if(err){
        console.error('Error last ID not saved :', err);
      }
    })
  }
}

const lii = new Watcher();
lii.initialize();

  //request('https://api.guildwars2.com/v2/items/41824', (error, status, body)=>{console.log(body)})
